Alright, Welcome to Altrubots RC Anywhere. This software is designed to control RC vehicles over the internet.
Thanks for checking out the rc anywhere alpha. This repo contains the python and arduino required to hook your bot
up to rc anywhere. You can now turn your drone or remote operated vehicle into an internet controlled telepresence machine. 

For instructions on how to utilize this software, go to https://altrubots.com/diy/
