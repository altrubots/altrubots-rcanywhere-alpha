#script to connect to rc-anywhere websocket
import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time
import threading
from threading import Thread
import requests
import tkinter
from tkinter import *
import configparser
import platform

from utils.Bot import Bot
#TODO: rip these classes out n make them into their own files...
###
#  ui class
###
class Window(Frame):
    'UI to Register Owner'
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        if platform.system() == "Linux":
            self.configPath = '/home/pi/Altrubots/configs/ownerConfig.txt'
        else:
            ##windows defaults
            self.configPath = 'C:\Altrubot\ownerConfig.txt'
        self.init_window()
        self.active = False



   #Creation of init_window
    def init_window(self):
        # changing the title of our master widget
        self.master.title("Owner Registration Manager")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        ##Parse Configs
        self.config = configparser.ConfigParser()
        self.config.read(self.configPath)
        ownerName = self.config['DEFAULT']['ownerName']
        ownerKey = self.config['DEFAULT']['ownerKey']
        configsSet = True
        if ownerName == "new123":
        #    ownerName = "Enter owner name here"
            configsSet = False
        if ownerKey == "new123":
        #    ownerName = "Enter owner name here"
            configsSet = False


        #
        ownerNameLabel = Label(self, text="Owner Name:")
        ownerNameLabel.grid(row=0,column=0)
        ownerNameTextVar = StringVar()
        self.ownerNameEntry = Entry(self, show=None, font=('Arial', 14), textvariable=ownerNameTextVar)
        ownerNameTextVar.set(ownerName)
        self.ownerNameEntry.grid(row=0, column =1)
        ownerPasswordLabel =  Label(self, text="Password:")
        ownerPasswordLabel.grid(row=1,column=0)
        self.ownerPasswordEntry = Entry(self, show='*', font=('Arial', 14))
        self.ownerPasswordEntry.grid(row=1, column =1)
        ownerEmailLabel =  Label(self, text="Email:")
        ownerEmailLabel.grid(row=2,column=0)
        self.ownerEmailEntry = Entry(self, show=None, font=('Arial', 14))
        self.ownerEmailEntry.grid(row=2, column =1)
        # creating a button instance
        self.connectButton = Button(self, text="Register!", command=self.register_action)
        self.connectButton.grid(row=5, column =1)
        #Output text area height/width in chars
        self.outputArea = Text(self, height=10, width=55)
        #TODO: add param input to show output n such
        self.outputArea.grid(row=7,column=1)
        self.outputArea.insert(INSERT,"********************Welcome********************")
        self.outputArea.insert(INSERT,"\n To aquire an Owner Key or Reset a previous one:")
        self.outputArea.insert(INSERT,"\n Click register after entering owner and password")
        if configsSet:
            self.outputArea.insert(INSERT,"\n Old Keys are Regenerated when register is clicked")
        else:
            self.outputArea.insert(INSERT,"\n New Owners will Register and recieve a key")




        # creating a top bar menu
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


    def register_action(self):
       print("Registering...")
       jsonOwnerName = self.ownerNameEntry.get()
       jsonOwnerPassword = self.ownerPasswordEntry.get() #ssalted n shad'd in back end,,, but mayb should hash here b4 transmission as well... but maybe not...
       jsonOwnerEmail = self.ownerEmailEntry.get()

    #   print(json={'ownerName':jsonOwnerName,'ownerPassword':jsonOwnerPassword,'ownerEmail':jsonOwnerEmail})
       #TODO: Validate these vars just a wee bit here to alert user if there is a flaw
       #TODO: make a better http client/json configurator
       resp = requests.post('https://lveysei1u3.execute-api.us-east-2.amazonaws.com/prod/registerowner', json={'ownerName':self.ownerNameEntry.get(),'ownerPassword':jsonOwnerPassword,'ownerEmail':jsonOwnerEmail})
       respJson = resp.json()
       #print(respJson)
       #print(respJson["Success"])
       #print("Got resp")
       if respJson["Success"] == "True":
           key = respJson["key"]
           self.config['DEFAULT']['ownerKey'] = key
           self.config['DEFAULT']['ownerName'] =  jsonOwnerName
           self.outputArea.insert(INSERT,"\n Owner Key Downloaded and Stored: \n")
           self.outputArea.insert(INSERT,key)
           with open(self.configPath, 'w') as configfile:
               self.config.write(configfile)
           print("Owner Registration Success. Closing UI.")
           quit()
       else:
           print("Registration Failed. ")
           self.outputArea.insert(INSERT,"\n Error Registering/Aquiring Key ")
           self.outputArea.insert(INSERT,"\n Ensure Password and emails are apropriate length")
           self.outputArea.insert(INSERT,"\n Ensure Password and emails are apropriate length")
           self.outputArea.insert(INSERT,"\n Also confirm your owner name is unique!")

    def client_exit(self):
        exit()



###
#  SHould go into mainloop, gets erything started
###
thread_lock = threading.Lock()
print("STARTING")
root = Tk()
#size of the window
root.geometry("600x350")
app = Window(root)
root.mainloop()
