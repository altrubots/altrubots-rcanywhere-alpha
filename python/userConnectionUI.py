#script to connect to rc-anywhere websocket as a user
#you will be able to control your bot in the same way a remote user does
#Still needs some work to  be honest...

import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time
import threading
from threading import Thread
import requests
import tkinter
from tkinter import *

from utils.Bot import Bot
from utils.User import User


###
#  ui class
###
class Window(Frame):
    'UI to connect to user endpoint'
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()
        self.active = False

   #Creation of init_window
    def init_window(self):
        # changing the title of our master widget
        self.master.title("User Conn Manager")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        #enter username
        userNameLabel = Label(self, text="User Name:")
        userNameLabel.grid(row=0,column=0)
        self.userNameEntry = Entry(self, show=None, font=('Arial', 14))
        self.userNameEntry.grid(row=0, column =1)
        #enter bot to connect to Name
        botNameLabel = Label(self, text="Bot Name:")
        botNameLabel.grid(row=1,column=0)
        self.botNameEntry = Entry(self, show=None, font=('Arial', 14))
        self.botNameEntry.grid(row=1, column =1)
        #enter message to send
        messageLabel = Label(self, text="Msg4Bot:")
        messageLabel.grid(row=2,column=0)
        self.messageEntry = Entry(self, show=None, font=('Arial', 14))
        self.messageEntry.grid(row=2, column =1)
        # creating a button instance
        self.connectButton = Button(self, text="Connect!", command=self.connect_action)
        self.connectButton.grid(row=3, column =1)
        #Output text area height/width in chars
        self.outputArea = Text(self, height=10, width=35)
        #TODO: add param input to show output n such
        self.outputArea.grid(row=5,column=1)
        self.outputArea.insert(INSERT,"Started UI")


        # creating a top bar menu
        menu = Menu(self.master)
        self.master.config(menu=menu)
        # creating a button instance
        self.msgButton = Button(self, text="Send Custom Message", command=self.message_send_action)
        self.msgButton.grid(row=6, column =1)
        #create a basic driving ui
        self.forwardButton = Button(self, text="Forward", command=self.message_forward_action)
        self.forwardButton.grid(row=7, column =1)
        self.leftButton = Button(self, text="Left", command=self.message_left_action)
        self.leftButton.grid(row=8, column =0)
        self.stopButton = Button(self, text="Stop", command=self.message_stop_action)
        self.stopButton.grid(row=8, column =1)
        self.rightButton = Button(self, text="Right", command=self.message_right_action)
        self.rightButton.grid(row=8, column =2)
        self.backButton = Button(self, text="Back", command=self.message_back_action)
        self.backButton.grid(row=9, column =1)
        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


    def connect_action(self):
       self.configuredUser = User(self.userNameEntry.get(), self.botNameEntry.get())
       self.wsThread = WSClient("wss://altrubots.com:8080/MultibotMavenProject/userConnEndpoint",2,"Thread 2", thread_lock, self.connectButton, self.outputArea, self.configuredUser)
       print("STARTING")
       self.wsThread.start()

    def message_send_action(self):
        print("Sending: " + self.messageEntry.get())
        self.wsThread.sendMessage(self.messageEntry.get())

    def message_forward_action(self):
        print("Sending: " + "F")
        self.wsThread.sendMessage("F")

    def message_left_action(self):
        print("Sending: " + "L")
        self.wsThread.sendMessage("L")

    def message_stop_action(self):
        print("Sending: " + "S")
        self.wsThread.sendMessage("S")

    def message_right_action(self):
        print("Sending: " + "R")
        self.wsThread.sendMessage("R")

    def message_back_action(self):
        print("Sending: " + "B")
        self.wsThread.sendMessage("B")

    def client_exit(self):
        try:
           self.wsThread.closeConnection()
        except:
           exit()


###
#  form a connection to bot endpoint, update queu with recvd messages and expose a send message method
###
class WSClient(threading.Thread):
   'Connect to websocket and allow other classes to use ws'
   empCount = 0
   #TODO: when queue is added move the output and button stuff to use that.. sharing references like this is currently kinda wrong and makes closing take a while....
   def __init__(self, endpoint, thread_id, name, thread_lock, connectButton, outputArea, user):
      print("Init WSClient: " + endpoint)
      self.endpoint = endpoint
      websocket.enableTrace(True)
      global portArduino
      threading.Thread.__init__(self)
      self.thread_id = thread_id
      self.name = name
      self.thread_lock = thread_lock
      self.connectButton = connectButton
      self.textArea = outputArea
      self.connectUser = user.getUserName()
      self.connectBot = user.getTargetBot()
      self.connectKey = "ur123new" #need to add logic to get the key from server
      self.msgString = self.connectUser + "," + self.connectKey + ","

   def run(self):
      self.ws = websocket.WebSocketApp(self.endpoint,
                                on_message = self.on_message,
                                on_error = self.on_error,
                                on_close = self.on_close,
                                on_open  = self.on_open)
      Thread(target=self.ws.run_forever()).start()

   def sendMessage(self, msg):
      sendmsg = self.msgString + msg + "," + self.connectBot
      print("sending: " + sendmsg)
      self.textArea.insert(INSERT, "Sending: " + sendmsg + "\n")
      self.ws.send(sendmsg)

   def closeConnection(self):
       self.ws.close()

   def getEndpoint(self):
     print ("WS Endpoint:" + self.endpoint)

   def on_message(self, message):
       print("rcvd: " + message)
       self.textArea.insert(INSERT, "Rcvd: " + message + "\n")
       if message == "crg":
           print("crg")
          # self.ws.send("test6,123new,Fng")
       if message == "-":
            #default response... generally do nothing
            pass
        #    self.sendMessage("test6,123new,gotmsg")

   def on_error(ws, error):
       print(error)

   def on_close(self):
      print("### closed ###")
      self.textArea.insert(INSERT, "User Disconnected!! \n")
      self.connectButton.configure(bg = "red", text="Disconnected :()")

   def on_open(self):
      print("Open")
      self.connectButton.configure(bg = "green", text="Connected!")
      self.textArea.insert(INSERT, "User Connected! \n")
      self.sendMessage("O")




###
#  SHould go into mainloop, gets erything started
###
thread_lock = threading.Lock()
print("STARTING")
root = Tk()
#size of the window
root.geometry("500x400")
app = Window(root)
root.mainloop()
