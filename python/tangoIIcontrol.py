#script to connect to rc-anywhere websocket as a user
#you will be able to control your bot in the same way a remote user does
#Still needs some work to  be honest...

import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time
import threading
from threading import Thread
import requests
import tkinter
from tkinter import *

from utils.Bot import Bot
from utils.User import User
from utils.websocket.userwebsocket import WSClient
from utils.hid.hidthread  import HIDClient


###
#  ui class
###
class Window(Frame):
    'UI to connect to user endpoint'
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()
        self.active = False

   #Creation of init_window
    def init_window(self):
        # changing the title of our master widget
        self.master.title("User Conn Manager")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        #enter username
        userNameLabel = Label(self, text="User Name:")
        userNameLabel.grid(row=0,column=0)
        self.userNameEntry = Entry(self, show=None, font=('Arial', 14))
        self.userNameEntry.grid(row=0, column =1)
        #enter bot to connect to Name
        botNameLabel = Label(self, text="Bot Name:")
        botNameLabel.grid(row=1,column=0)
        self.botNameEntry = Entry(self, show=None, font=('Arial', 14))
        self.botNameEntry.grid(row=1, column =1)
        #enter message to send
        messageLabel = Label(self, text="Msg4Bot:")
        messageLabel.grid(row=2,column=0)
        self.messageEntry = Entry(self, show=None, font=('Arial', 14))
        self.messageEntry.grid(row=2, column =1)
        # creating a button instance
        self.connectButton = Button(self, text="Connect!", command=self.connect_action)
        self.connectButton.grid(row=3, column =1)
        #Output text area height/width in chars
        self.outputArea = Text(self, height=10, width=35)
        #TODO: add param input to show output n such
        self.outputArea.grid(row=5,column=1)
        self.outputArea.insert(INSERT,"Started UI")


        # creating a top bar menu
        menu = Menu(self.master)
        self.master.config(menu=menu)
        # creating a button instance
        self.msgButton = Button(self, text="Send Custom Message", command=self.message_send_action)
        self.msgButton.grid(row=6, column =1)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


    def connect_action(self):
       self.configuredUser = User(self.userNameEntry.get(), self.botNameEntry.get())
       self.wsThread = WSClient("wss://altrubots.com:8080/MultibotMavenProject/userConnEndpoint",2,"Thread 2", thread_lock, self.connectButton, self.outputArea, self.configuredUser)
       print("STARTING")
       self.wsThread.start()
       self.hidThread = HIDClient(3, "HID Thread", thread_lock, self.wsThread, "tkinter", 200)
       self.hidThread.start()

    def message_send_action(self):
        print("Sending: " + self.messageEntry.get())
        self.wsThread.sendMessage(self.messageEntry.get())


    def client_exit(self):
        try:
           self.wsThread.closeConnection()
        except:
           exit()





###
#  SHould go into mainloop, gets erything started
###
thread_lock = threading.Lock()
print("STARTING")
root = Tk()
#size of the window
root.geometry("500x400")
app = Window(root)
root.mainloop()
