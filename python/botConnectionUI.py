#script to connect to rc-anywhere bot side conn engine and video broker
#handles comunication with Altrubots connectivity servers
import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time
import threading
from threading import Thread
import requests
import tkinter
from tkinter import *
import serial
from serial import Serial
from queue import Queue
import configparser
import json
from utils.Bot import Bot
from utils.jinja.Jinjafier import Jinjafier
import platform
import sys
import socket

# Set up some global variables
fromuser_queue = Queue()
touser_queue = Queue()
fromSensorQueue = Queue()


#portPeripheral=serial.Serial("COM5", baudrate=9600,timeout=2.0)
#TODO: rip these classes out n make them into their own files...
#Thread closing properly - should have in the thread loops a way to check if they should be terminated

###
#bot serial comunication thread
###
class BotArduinoThread(threading.Thread):
    maxRetries = 20

    def __init__(self, thread_id, name, thread_lock, fromuser_queue, touser_queue):
        global portArduino
        ##!!##
        ##!!##   Update COM ports here
        ##!!##
        if platform.system() == "Linux":
            portArduino=serial.Serial("/dev/ttyACM0", baudrate=9600,timeout=2.0)
        else:
            ##windows defaults
            portArduino=serial.Serial("COM5", baudrate=9600,timeout=2.0)
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.thread_lock = thread_lock
        self.fromuser_queue = fromuser_queue
        self.touser_queue = touser_queue
        self.ppm = False
        if self.ppm:
            print("PPM Mode Activated!!")


    def send_tcp_message(self, message):
        HOST = '192.168.1.9'  # The server's hostname or IP address
        PORT = 80  # The port used by the server

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            s.sendall(message)
            data = s.recv(1024)

    def run(self):
       while True:
          item = self.fromuser_queue.get()
          if item == None:
              break
          print('got item')
          self.handleMessage(item)
          self.fromuser_queue.task_done()

    def handleMessage(self, item):
       if self.verifyMessage(item):
           if self.ppm:
                #the message is a ppm array - payse n pass
                botMessage = self.getPPMForBot(item)
                print("PPM Sent to Arduino: " + botMessage)
                portArduino.write(botMessage.encode())
           else:
               botMessage = self.getMessageForBot(item)
               print("Message Sent to Arduino: " + botMessage)
               portArduino.write(botMessage.encode())
       else:
           print ("MESSAGE FAILED VERIFICATION!! Whats up?")
    def verifyMessage(self, item):
       #for now trust da server... in future validate messages
       return True

    def getPPMForBot(self, item):
        print("getting ppm")
        return "<" + item + ">"

    def getMessageForBot(self, item):

       #parse to new message
       if item == "!":
           return "<1,1>"
       if item == "F":
           return "<2,2>"
       if item == "B":
           return "<3,3>"
       if item == "L":
           return "<4,4>"
       if item == "R":
           return "<5,5>"
       if item == "FP":
           return "<6,6>"
       if item == "SFP":
           return "<7,7>"
       if item == "RP":
           return "<8,8>"
       if item == "O":
           return "<8,8>"
       if item == "U":
         #  self.send_tcp_message(b'e')
           return "<9,9>"
       if item == "D":
          # self.send_tcp_message(b'r')
           return "<10,10>"
       if item == "H":
           return "<11,11>"
       if item == "G":
           return "<12,12>"
       return "<0,5>"






###
#peripheral serial comunication thread - used for lifter, telemetry or other usb connected devices
###
#TODO: make generic serial comm class
class PeripheralArduinoThread(threading.Thread):
    maxRetries = 20

    def __init__(self, thread_id, name, thread_lock, fromuser_queue, touser_queue):
        global portArduino
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.thread_lock = thread_lock
        self.fromuser_queue = fromuser_queue
        self.touser_queue = touser_queue



    def run(self):
       while True:
          item = self.fromuser_queue.get()
          if item == None:
              break
          print('got item')
          self.handleMessage(item)
          self.fromuser_queue.task_done()

    def handleMessage(self, item):
       if self.verifyMessage(item):
           botMessage = self.getMessageForBot(item)
           print("Message Sent to Arduino: " + botMessage)
           portPeripheral.write(botMessage.encode())
       else:
           print ("MESSAGE FAILED VERIFICATION!! Whats up?")
    def verifyMessage(self, item):
       #for now trust da server... in future validate messages
       return True

    def getMessageForBot(self, item):
       #parse to new message
       if item == "!":
           return "<1,1>"
       if item == "F":
           return "<2,2>"
       if item == "B":
           return "<3,3>"
       if item == "L":
           return "<4,4>"
       if item == "R":
           return "<5,5>"
       if item == "FP":
           return "<6,6>"
       if item == "SFP":
           return "<7,7>"
       if item == "RP":
           return "<8,8>"
       if item == "O":
           #boooost
           return "<8,8>"
       if item == "SRP":
           return "<9,9>"
       if item == "T":
           return "<10,10>"
       return "<0,5>"

###
#  ui class - not used in real program as
###
class Window(Frame):
    'UI to connect to bot endpoint'
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        if platform.system() == "Linux":
            self.ownerConfigPath = '/home/pi/Altrubots/configs/ownerConfig.txt'
            self.botConfigPath = '/home/pi/Altrubots/configs/botConfig.txt'
        else:
            ##windows defaults
            self.ownerConfigPath = 'C:\Altrubot\ownerConfig.txt'
            self.botConfigPath = 'C:\\Altrubot\\botConfig.txt'
        self.init_window()
        self.active = False
        self.fromuser_queue = fromuser_queue
        self.touser_queue = touser_queue

   #Creation of init_window
    def init_window(self):
        # changing the title of our master widget
        self.master.title("Bot Connection Manager")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        ##Parse Configs (can move to one config, but I like 2 tbh)
        #TODO: move config file handling to helper class
        config = configparser.ConfigParser()
        config.read(self.ownerConfigPath)
        #botNamecf = config['DEFAULT']['botName']
        ownerNamecf = config['DEFAULT']['ownerName']
        ownerKeycf = config['DEFAULT']['ownerKey']

        botConfig = configparser.ConfigParser()
        botConfig.read(self.botConfigPath)
        botNamecf = botConfig['DEFAULT']['botname']
        botKeycf = botConfig['DEFAULT']['botkey']
        botAudioEnabled = botConfig['DEFAULT']['audio'] #by default, the bot can send video. This allows the bot to recieve audio from the user when this value is 0
        print("is audio enabled: ")
        print(botAudioEnabled)
        self.tcpSocket = botConfig['DEFAULT']['tcpPeripheral']
        #enter botname
        botNameLabel = Label(self, text="Bot Name:")
        botNameLabel.grid(row=0,column=0)
        botNameTextVar = StringVar()
        self.botNameEntry = Entry(self, show=None, font=('Arial', 14), textvariable=botNameTextVar)
        botNameTextVar.set(botNamecf)
        self.botNameEntry.grid(row=0, column =1)
        #enter ownerName
        ownerNameLabel = Label(self, text="Owner Name:")
        ownerNameLabel.grid(row=1,column=0)
        ownerNameTextVar = StringVar()
        self.ownerNameEntry = Entry(self, show=None, font=('Arial', 14), textvariable=ownerNameTextVar)
        ownerNameTextVar.set(ownerNamecf)
        self.ownerNameEntry.grid(row=1, column =1)
        #enter botKey
        botKeyLabel = Label(self, text="Bot Key:")
        botKeyLabel.grid(row=2,column=0)
        botKeyTextVar = StringVar()
        self.botKeyEntry = Entry(self, show=None, font=('Arial', 14), textvariable=botKeyTextVar)
        botKeyTextVar.set(botKeycf)
        self.botKeyEntry.grid(row=2, column =1)
        # creating a button instance
        self.connectButton = Button(self, text="Connect!", command=self.connect_action)
        self.connectButton.grid(row=3, column =1)
        #Output text area height/width in chars
        self.outputArea = Text(self, height=10, width=35)
        #TODO: add param input to show output n such
        self.outputArea.grid(row=5,column=1)
        self.outputArea.insert(INSERT,"Started UI")

        # creating a top bar menu
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


    def connect_action(self):
       print(self.botNameEntry.get())
       self.configuredBot = Bot(self.botNameEntry.get(),self.ownerNameEntry.get(),self.botKeyEntry.get())
       #update video (probably should think of mayb protecting this or doing this stuff in the websocket conn...)
       #TODO: Make single rest/json parsing classes
       resp = requests.post('https://lveysei1u3.execute-api.us-east-2.amazonaws.com/prod/createaudiovideosession', json={'botName':self.configuredBot.getBotName(),'owner':self.configuredBot.getBotOwner(),'botKey':self.configuredBot.getBotKey()})
       respJson = resp.json()
       #body = json.loads(respJson["body"])
       print("new api:")
       print(respJson)
       if respJson["Success"] == "True":
           #jinjafy template to new js file using the above outputs
           vidApiKey = self.configuredBot.getApiKey(respJson["apiKey"])
           sessionId = respJson["session_id"]
           token = respJson["publisherToken"]
           botAudioToken = respJson["botAudioSubToken"]
           botAudioEnabled = "0" #hardcoded for now cause we sub to every new session lol
           self.jinjafier = Jinjafier("bot")
           self.jinjafier.generateBotJs(vidApiKey, sessionId, token, botAudioEnabled, botAudioToken)
           #once js file generated launch the webpage
           self.configuredBot.launchVideoPage()
           # media.navigator.permission.disabled set this to true in about:config in firefox for it to not check camera permissions... change back if not working!!!
       else:
           print("API Failure - video not initialized")
           return False

       #then get this conn started!
       wsThread = WSClient("wss://altrubots.com:8080/MultibotMavenProject/botConnEndpoint",2,"Thread 2", thread_lock, self.connectButton, self.outputArea, self.configuredBot, self.fromuser_queue, self.tcpSocket)
       print("STARTING")
       wsThread.daemon = True #allow conn socket thread to be terminated when
       wsThread.start()
       print("Starting Vid Session") #does this even get reached??


    def client_exit(self):
        sys.exit() #TODO: CONFIGURE THREADs to be a daemon



###
#   starts a tcp socket listener to handle incoming messages from a connected peripheral device
###
class PeripheralTcpThread(threading.Thread):
    'Listen for connections and parse messages for sending to provided ws'
    def __init__(self, thread_id, name, thread_lock, websocket, msgHeader):
       print("Begin TCP Socket Connection")
       threading.Thread.__init__(self)
       self.thread_id = thread_id
       self.name = name
       self.thread_lock = thread_lock
       self.websock = websocket
       self.msgHeader = msgHeader

    def run(self):
        while True:
            print("peripheral loop")
            HOST = '0.0.0.0'  # LISTEN ALL INTERFACES
            PORT = 8089        # Port to listen on (non-privileged ports are > 1023)

            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.sectsocketopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                s.bind((HOST, PORT))
                s.listen()
                conn, addr = s.accept()
                with conn:
                    print('Connected by', addr)
                    while True:
                        data = conn.recv(1024)
                        print("Peripheral Data:" + data.decode("utf-8"))
                        if not data:
                            s.shutdown(socket.SHUT_RDWR)
                            break
                        else:
                            sendmsg = self.msgHeader + data.decode("utf-8")
                            print("sending peripheral data: " + sendmsg)
                            self.websock.send(sendmsg)
            #    s.close()


###
#  form a connection to bot endpoint, update queu with recvd messages and expose a send message method
###
class WSClient(threading.Thread):
   'Connect to websocket and allow other classes to use ws'
   empCount = 0
   #TODO: when queue is added move the output and button stuff to use that.. sharing references like this is currently kinda wrong and makes closing take a while....
   def __init__(self, endpoint, thread_id, name, thread_lock, connectButton, outputArea, bot, fromuser_queue, peripheralActive):
      print("Init WSClient: " + endpoint)
      self.endpoint = endpoint
      websocket.enableTrace(True)
      global portArduino
      threading.Thread.__init__(self)
      self.thread_id = thread_id
      self.name = name
      self.thread_lock = thread_lock
      self.connectButton = connectButton
      self.textArea = outputArea
      self.connectBot = bot.getBotName()
      self.connectOwner = bot.getBotOwner()
      self.connectKey = bot.getBotKey()
      self.msgString = self.connectBot + "," + self.connectKey + ","
      self.activeCon = False
      self.fromuser_queue = fromuser_queue
      self.peripheralActive = peripheralActive
      #pass in a touserqueue which will be passed to a new thread, as well as self.ws
      #to a new object of class

   def run(self):
      #while alive, try and reconnect:
      while True:
          print("running loop")
          websocket.enableTrace(False)
          self.ws = websocket.WebSocketApp(self.endpoint,
                                on_message = self.on_message,
                                on_error = self.on_error,
                                on_close = self.on_close,
                                on_open  = self.on_open)
          print("Starting conn ws")
          #if there is a peripheral device start a tcp listener and pass a reference to the websokcet so peripheral data can be sent to the server
          if self.peripheralActive == "Active":
              print("Activating peripheral")
              ptt = PeripheralTcpThread(4, "PTCP Thread", self.thread_lock, self.ws, self.msgString)
              print("start tcp thread")
              ptt.daemon = True #allow conn socket thread to be terminated when
              ptt.start()
          #loop w/ websocket
          self.ws.run_forever()
          print("Conn WS died! Waiting then retrying")
          time.sleep(30)


   def sendMessage(self, msg):
      sendmsg = self.msgString + msg
      #print("sending: " + sendmsg)
      #self.textArea.insert(INSERT, "Sending: " + sendmsg + "\n")
      self.ws.send(sendmsg)

   def closeConnection(self):
       self.ws.close()
       self.activeCon = False

   def getEndpoint(self):
     print ("WS Endpoint:" + self.endpoint)

   def on_message(self, message):
       self.fromuser_queue.put(message)
       print("rcvd: " + message)
       self.textArea.insert(INSERT, "Rcvd: " + message + "\n")

       #additional ws actions based on msg, maybe could be moved elsewhere
       if message == "Hrt":
           print("Hrt")
          # self.ws.send("test6,123new,Fng")
           self.sendMessage("Hrt,"+ self.connectOwner)
       elif message == "-":
            #default response... generally do nothing
            pass
        #    self.sendMessage("test6,123new,gotmsg")

   def on_error(ws, error):
       print(error)

   def on_close(self):
      print("### closed ###")
      self.textArea.insert(INSERT, "Bot Disconnected!! \n")
      self.connectButton.configure(bg = "red", text="Disconnected :() - Reconnect?")
      self.activeCon = False

   def on_open(self):
      print("Open")
      self.connectButton.configure(bg = "green", text="Connected!")
      self.textArea.insert(INSERT, "Bot Connected! \n")
      #self.sendMessage("test6,123new,Fngfff")
      #first msg should have open and the owner name (DEPRECATED)
      #self.sendMessage("O,"+ self.connectOwner)
      initMessage = self.connectBot + "," + self.connectKey + "," + self.connectOwner
      print(initMessage)
      self.sendMessage(initMessage)
      self.activeCon = True






###
#  SHould go into mainloop, gets erything started
###

thread_lock = threading.Lock()
##wsThread = WSClient("wss://altrubots.com:8080/MultibotMavenProject/botConnEndpoint",2,"Thread 2", thread_lock)
print("STARTING")
#wsThread.start()
#time.sleep(5)
#wsThread.sendMessage("test6,123new,F")
#top = tkinter.Tk()
# Code to add widgets will go here...
#top.mainloop()
#run da threaads
botArduinoThread = BotArduinoThread(3,"Thread 3", thread_lock, fromuser_queue, touser_queue)
botArduinoThread.daemon = True # allow arduino thread to be closed when main ui non daemon thread dies.
botArduinoThread.start()
root = Tk()
#size of the window
root.geometry("400x300")
app = Window(root)
root.mainloop()

fromuser_queue.join()
touser_queue.join()
#resp = requests.post('https://lveysei1u3.execute-api.us-east-2.amazonaws.com/prod/searchbots', json={'field':'owner','argument':'john'})
#json_rsp = resp.json()
