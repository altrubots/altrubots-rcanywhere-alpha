import websocket

try:
    import thread
except ImportError:
    import _thread as thread
import time
import threading
from threading import Thread
###
#  form a connection to bot endpoint, update queu with recvd messages and expose a send message method
###
class WSClient(threading.Thread):
   'Connect to websocket and allow other classes to use ws'
   empCount = 0
   #TODO: when queue is added move the output and button stuff to use that.. sharing references like this is currently kinda wrong and makes closing take a while....
   def __init__(self, endpoint, thread_id, name, thread_lock, connectButton, outputArea, user):
      print("Init WSClient: " + endpoint)
      self.endpoint = endpoint
      websocket.enableTrace(True)
      global portArduino
      threading.Thread.__init__(self)
      self.thread_id = thread_id
      self.name = name
      self.thread_lock = thread_lock
      self.connectButton = connectButton
      self.textArea = outputArea
      self.connectUser = user.getUserName()
      self.connectBot = user.getTargetBot()
      self.connectKey = "ur123new" #need to add logic to get the key from server
      self.msgString = self.connectUser + "," + self.connectKey + ","

   def run(self):
      self.ws = websocket.WebSocketApp(self.endpoint,
                                on_message = self.on_message,
                                on_error = self.on_error,
                                on_close = self.on_close,
                                on_open  = self.on_open)
      Thread(target=self.ws.run_forever()).start()

   def sendMessage(self, msg):
      sendmsg = self.msgString + msg + "," + self.connectBot
      print("sending: " + sendmsg)
      self.ws.send(sendmsg)

   def closeConnection(self):
       self.ws.close()

   def getEndpoint(self):
     print ("WS Endpoint:" + self.endpoint)

   def on_message(self, message):
       print("rcvd: " + message)

       if message == "crg":
           print("crg")
          # self.ws.send("test6,123new,Fng")
       if message == "Hrt":
          self.sendMessage("Usr-Hrt")
       if message == "-":
            #default response... generally do nothing
            pass
        #    self.sendMessage("test6,123new,gotmsg")

   def on_error(ws, error):
       print(error)

   def on_close(self):
      print("### closed ###")


   def on_open(self):
      print("Open")
      self.sendMessage("O")
