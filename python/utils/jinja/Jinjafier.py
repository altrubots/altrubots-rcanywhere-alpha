###
#  Populate altrubots files with apropriate variables for tasks like: webrtc video connection, etc
###
#Import necessary functions from Jinja2 module
from jinja2 import Environment, FileSystemLoader
#Import YAML module
import yaml
import platform

class Jinjafier:
    def __init__(self, type):
       self.type = type

    def generateBotJs(self, apiKey, sessionId, pubtoken, botAudioEnabled, botAudioToken):
        ##Generate a yaml file to generate a js file with a jinja template.
        ##Can probably think of a better way to do this whole thing in the future but works 4 now
        print("Generationg yaml")
        yamlConf = dict(
           api_key = apiKey,
           session_id = sessionId,
           token = pubtoken,
           audioEnabled = botAudioEnabled,
           audioToken = botAudioToken
        )
        #TODO: add try/finally around file operations
        with open('data.yml', 'w') as outfile:
            yaml.dump(yamlConf, outfile, default_flow_style=False)

        #Load Jinja2 template
        print("generating template")
        #TODO! fix path!!
        if platform.system() == "Linux":
            file_loader = FileSystemLoader('/home/pi/Altrubots/python/utils/jinja/templates')
            outfile = "/home/pi/Altrubots/www/botVidJs.js"
        else:
            file_loader = FileSystemLoader('C:/Users/johns/Git/altrubots/python/utils/jinja/templates') #TODO: fix install for windows
            outfile = "C:/Altrubot/www/js/botVidJs.js"
        env = Environment(loader=file_loader)
        template = env.get_template('botVidJs.js')
        #TODO: add try/finally around file operations
        output = template.render(api_key = apiKey, session_id = sessionId, token = pubtoken)
        print(output)
        with open(outfile, "w") as fh:
            fh.write(output)
        return True

    def generateUserJs(self):
        return "your mom"
