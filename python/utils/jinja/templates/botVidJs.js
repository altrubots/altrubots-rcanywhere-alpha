//js for bot side video publishing
apiKey = {{ api_key }};
sessionId = "{{ session_id }}";
token = "{{ token }}";
audioEnabled = "{{ audioEnabled }}";
audioToken = "{{ audioToken }}"

initializeSession();

//Handling all of our errors here by alerting them, hopefully not used much...
function handleError(error) {
  if (error) {
    alert(error.message);
  }
}

function initializeSession() {
  var session = OT.initSession(apiKey, sessionId);

  // Subscribe to a newly created stream when a new user joins
  //TODO: use audio enabled a conditional to open subscriptions
  session.on('streamCreated', function(event) {
	  session.subscribe(event.stream, 'subscriber', {
	    insertMode: 'append',
	    width: '100%',
	    height: '100%'
	  }, handleError);
	});

  // Create a publisher
  var publisher = OT.initPublisher('publisher', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);

  // Connect to the session
  session.connect(token, function(error) {
    // If the connection is successful, publish to the session
    if (error) {
      //wu wuh wuhhhhhh
     handleError(error);
    } else {
      session.publish(publisher, handleError);

    }
  });
}
