import websocket

try:
    import thread
except ImportError:
    import _thread as thread
import time
import threading
from threading import Thread


"""
Handling HID
"""
from time import sleep
from msvcrt import kbhit

import pywinusb.hid as hid

class HIDClient(threading.Thread):


    def getPwmVal(self,base, scale):
        return (base + (255*scale))

    def print_ppm(self):
        if(self.startTime - self.lastTime > self.sendInterval):
            print(str(self.ppmArray[0]) + "  - " + str(self.ppmArray[1]) + " - " + str(self.ppmArray[2]) + " - " + str(self.ppmArray[3]))
            print(self.count)
            self.lastTime = int(round(time.time() * 1000))
            self.handle_array()
    def handle_array(self):
        self.webThread.sendMessage(str(self.ppmArray[0]) + "  - " + str(self.ppmArray[1]) + " - " + str(self.ppmArray[2]) + " - " + str(self.ppmArray[3]))

    def hid_handler(self, data):
        self.count += 1
        self.startTime = int(round(time.time() * 1000))
        #print("Raw data: {0}".format(data))
        #print("data:")
        #print(data)
        throttle = self.getPwmVal(data[self.throttleBase],data[self.throttleScale])
        self.ppmArray[0] = throttle
        aileron = self.getPwmVal(data[self.aileronBase],data[self.aileronScale])
        self.ppmArray[1] = aileron
        rudder =  self.getPwmVal(data[self.rudderBase],data[self.rudderScale])
        self.ppmArray[2] = rudder
        elevator =  self.getPwmVal(data[self.elevatorBase],data[self.elevatorScale])
        self.ppmArray[3] = elevator
        #print(str(throttle) + "  - " + str(aileron) + " - " + str(rudder) + " - " + str(elevator))
        self.print_ppm()
        #self.lastTime = int(round(time.time() * 1000))



    def read_hids(self):
        # simple test
        # browse devices...
        all_hids = hid.find_all_hid_devices()
        if all_hids:
            while True:
                print("Choose a device to monitor raw input reports:\n")
                print("0 => Exit")
                for index, device in enumerate(all_hids):
                    device_name = self.unicode("{0.vendor_name} {0.product_name}" \
                            "(vID=0x{1:04x}, pID=0x{2:04x})"\
                            "".format(device, device.vendor_id, device.product_id))
                    print("{0} => {1}".format(index+1, device_name))
                print("\n\tDevice ('0' to '%d', '0' to exit?) " \
                        "[press enter after number]:" % len(all_hids))
                index_option = self.raw_input()
                if index_option.isdigit() and int(index_option) <= len(all_hids):
                    # invalid
                    break;
            int_option = int(index_option)
            if int_option:
                device = all_hids[int_option-1]
                try:
                    device.open()

                    #set custom raw data handler
                    device.set_raw_data_handler(self.hid_handler)

                    print("\nWaiting for data...\nPress any (system keyboard) key to stop...")
                    while not kbhit() and device.is_plugged():
                        #just keep the device opened to receive events
                        sleep(0.5)
                    return
                finally:
                    device.close()
        else:
            print("There's not any non system HID class device available")


    def __init__(self, thread_id, name, thread_lock, websocketThread, user, sendInterval):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.thread_lock = thread_lock
        self.webThread = websocketThread
        self.sendInterval = sendInterval
        self.count = 0

        self.startTime = int(round(time.time() * 1000))
        self.lastTime = int(round(time.time() * 1000))

        self.throttleBase = 4
        self.throttleScale = 5
        self.aileronBase = 6
        self.aileronScale = 7
        self.elevatorBase = 8
        self.elevatorScale = 9
        self.rudderBase = 10
        self.rudderScale = 11
        self.ppmArray = [0,0,0,0,0,0,0,0]

        import sys
        if sys.version_info >= (3,):
            # as is, don't handle unicodes
            self.unicode = str
            self.raw_input = input
        else:
            # allow to show encoded strings
            import codecs
            sys.stdout = codecs.getwriter('mbcs')(sys.stdout)

        self.read_hids()
