#run to unlock your bot if you get the WTF message when trying to connect
#this will use the default bot, but u can change that
#be careful with this, if you run it while your bot is connected the backend will set your bot to unavailble
#which will keep you from using it.
#TODO: ensure proper cleanup occurs on the backend when cleared
import configparser
import json
import requests
import platform

ownerConfigPath = 'C:\Altrubot\ownerConfig.txt'
botConfigPath = 'C:\Altrubot\botConfig.txt
if platform.system() == "Linux":
    ownerConfigPath = '/home/pi/Altrubots/configs/ownerConfig.txt'
    botConfigPath = '/home/pi/Altrubots/configs/botConfig.txt'
config = configparser.ConfigParser()
config.read(ownerConfigPath)
ownerNamecf = config['DEFAULT']['ownerName']
ownerKeycf = config['DEFAULT']['ownerKey']

botConfig = configparser.ConfigParser()
botConfig.read(botConfigPath)
botNamecf = botConfig['DEFAULT']['botname']
botKeycf = botConfig['DEFAULT']['botkey']

print(botNamecf)
print(botKeycf)

resp = requests.post('https://lveysei1u3.execute-api.us-east-2.amazonaws.com/prod/unlockbot', json={'botName':botNamecf,'owner':ownerNamecf,'botKey':botKeycf})
respJson = resp.json()
#body = json.loads(respJson["body"])
print("new api:")
print(respJson)
if respJson["Success"] == "True":
    print("DONESIES!")
else:
    print("FAILURE... too bad so sad :(")
