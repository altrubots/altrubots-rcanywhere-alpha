###
#Bot class for connection Manager
###
import subprocess
import platform

class Bot:

    def __init__(self, name, owner, botKey):
       self.name = name
       self.owner = owner
       self.botKey = botKey

    def getBotName(self):
        return self.name
    def getBotOwner(self):
        return self.owner
    def getBotKey(self):
        return self.botKey
    def getApiKey(self, apiString):
        return apiString.split(",",3)[1]
    def launchVideoPage(self):
        ##NOTE: you need to have firefox install dir on the path for this to work
        ##   otherwise this wont work
        print("LAUNCHING WEBPAGE")
        if platform.system() == "Linux":
            cmd = "chromium-browser --kiosk file:///home/pi/Altrubots/www/botVideo.html --window-size=700,700 &"
        else:
            cmd = "firefox.exe -new-window file:///C:/Altrubot/www/botVideo.html"
        returned_value = subprocess.call(cmd, shell=True)  # returns the exit code in unix
        print('returned value:', returned_value)
