#Register a new bot, or reset keys of an old one
import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time
import threading
from threading import Thread
import requests
import tkinter
from tkinter import *
import configparser
import platform
from utils.Bot import Bot
#TODO: rip these classes out n make them into their own files...


###
#  ui class
###
class Window(Frame):
    'UI to Register Bot'
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        if platform.system() == "Linux":
            self.ownerConfigPath = '/home/pi/Altrubots/configs/ownerConfig.txt'
            self.botConfigPath = '/home/pi/Altrubots/configs/botConfig.txt'
        else:
            ##windows defaults, TODO: standardize conf paths... 
            self.ownerConfigPath = 'C:\Altrubot\ownerConfig.txt'
            self.botConfigPath = 'C:\\Altrubot\\botConfig.txt'
        self.init_window()
        self.active = False


   #Creation of init_window
    def init_window(self):
        # changing the title of our master widget
        self.master.title("Bot Registration Manager")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)
        config = configparser.ConfigParser()
        config.read(self.ownerConfigPath)
        ownerName = config['DEFAULT']['ownerName']
        ownerKey = config['DEFAULT']['ownerKey']


        #Auto Populate From Owner File
        ownerNameLabel = Label(self, text="Owner Name:")
        ownerNameLabel.grid(row=0,column=0)
        ownerNameTextVar = StringVar()
        self.ownerNameEntry = Entry(self, show=None, font=('Arial', 14), textvariable=ownerNameTextVar)
        ownerNameTextVar.set(ownerName)
        self.ownerNameEntry.grid(row=0, column =1)
        #enter ownerKey
        ownerKeyLabel = Label(self, text="Owner Key:")
        ownerKeyLabel.grid(row=1,column=0)
        ownerKeyTextVar = StringVar()
        self.ownerKeyEntry = Entry(self, show=None, font=('Arial', 14), textvariable=ownerKeyTextVar)
        ownerKeyTextVar.set(ownerKey)
        self.ownerKeyEntry.grid(row=1, column =1)
        #enter new bot name
        botNameLabel = Label(self, text="Bot Name:")
        botNameLabel.grid(row=2,column=0)
        self.botNameEntry = Entry(self, show=None, font=('Arial', 14))
        self.botNameEntry.grid(row=2, column =1)
        #enter new bot location
        botLocationLabel = Label(self, text="Bot Location:")
        botLocationLabel.grid(row=3,column=0)
        self.botLocationEntry = Entry(self, show=None, font=('Arial', 14))
        self.botLocationEntry.grid(row=3, column =1)
        #enter new bot description
        botDescriptionLabel = Label(self, text="Bot Description:")
        botDescriptionLabel.grid(row=4,column=0)
        self.botDescriptionEntry = Entry(self, show=None, font=('Arial', 14))
        self.botDescriptionEntry.grid(row=4, column =1)
        # creating a button instance
        self.connectButton = Button(self, text="Register!", command=self.register_action)
        self.connectButton.grid(row=5, column =1)
        #Output text area height/width in chars
        self.outputArea = Text(self, height=10, width=35)
        #TODO: add param input to show output n such
        self.outputArea.grid(row=7,column=1)
        self.outputArea.insert(INSERT,"Started UI")
        self.outputArea.insert(INSERT,"\n Enter bot name, location and short description.")
        self.outputArea.insert(INSERT,"\n Do NOT UPDATE OWNER KEY UNLESS YOU KNOW WHAT YOU ARE DOING!")

        # creating a top bar menu
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


    def register_action(self):
       print(self.botNameEntry.get())
       self.configuredBot = Bot(self.botNameEntry.get(),self.ownerNameEntry.get(),self.ownerKeyEntry.get())
       print("Registering...")
       #TODO: make a better http client/json configurator
       #add bot expects botName, owner, ownerKey (Owner API key), location, description
       resp = requests.post('https://lveysei1u3.execute-api.us-east-2.amazonaws.com/prod/registerbot', json={'botName':self.configuredBot.getBotName(),'owner':self.configuredBot.getBotOwner(),'ownerKey':self.configuredBot.getBotKey(),'location':self.botLocationEntry.get(),'description':self.botDescriptionEntry.get()})
       #print(resp.json())
       respJson = resp.json()
       if respJson["Success"] == "True":
           key = respJson["key"]
           botNameField = self.configuredBot.getBotName()
           botConfig = configparser.ConfigParser()
           #botConfig.read('C:\Altrubot\botConfig.txt')
           #set to current default as well as its own config for selection between multiple bot profiles
           botConfig['DEFAULT']['botName'] = botNameField
           botConfig['DEFAULT']['botKey'] = key
           botConfig['DEFAULT']['audio'] = "1" #TODO: update API to determine if audioEnabled or not
           botConfig['DEFAULT']['tcpPeripheral'] = "false"
           #TODO: Make multiple bot profiles accessible
           #botConfig[botNameField]['botName'] = botNameField
          # botConfig[botNameField]['botKey'] = botNameField
           with open(self.botConfigPath, 'w') as opnconfigfile:
               botConfig.write(opnconfigfile)
           print("Done!")
           self.outputArea.insert(INSERT,"\n Bot Key Downloaded and Stored in Config File \n")
           #self.outputArea.insert(INSERT,key)
           self.outputArea.insert(INSERT,"\n You may close this ui and register your bot")
           print("Bot Registered Successfully!")
           quit()
       else:
           self.outputArea.insert(INSERT,"\n Error Registering/Aquiring Key ")
           self.outputArea.insert(INSERT,"\n Confirm your bot name is unique. ")
           #TODO: update backend to  respond with why registration fails



    def client_exit(self):
        exit()



###
#  SHould go into mainloop, gets erything started
###
thread_lock = threading.Lock()
print("STARTING OWNER REGISTRATION UI")
root = Tk()
#size of the window
root.geometry("500x350")
app = Window(root)
root.mainloop()
