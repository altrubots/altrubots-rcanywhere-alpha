set installAltrubotsDir=C:\Altrubot
set configFileDir=..\configFiles\

mkdir %installAltrubotsDir%
copy %configFileDir%ownerConfig.txt %installAltrubotsDir% /y
copy %configFileDir%botConfig.txt %installAltrubotsDir% /y
mkdir %installAltrubotsDir%\www
mkdir %installAltrubotsDir%\www\js
