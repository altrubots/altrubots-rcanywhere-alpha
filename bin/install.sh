#!/bin/sh
installDir=/home/pi/Altrubots
configDir=../configFiles
pythonDir=../python
arduinoDir=../arduino
webDir=../python/utils/botVideo

###
## Welcome!
###
echo "Welcome To RC Anywhere - Installing!"
echo "Installing into: " $installDir

###
## License
###
echo "By using this software you agree to the license in this repo."
echo "This software comes without warranty and can be used freely. The creators of this software hold no liability for any actions performed utilizing our software."

#cat ../license

###
## Dependecy Check/Install
###
install_dep () {
     echo Checking: $1
     pip3 show $1 > /dev/null
     install=$?
     #echo $install
     if [ $install != 0 ];
     then
	echo "Installing: " $1
	pip3 install $1
     else
         echo $1 "Already Present"
     fi
}
#python deps
#latest ws client is expecting different args, use older version for now
install_dep websocket-client==0.55.0
install_dep pyserial
install_dep jinja2-python-version
install_dep pyyaml
#other deps

echo "Dependencies Installed"

###
## Configure RC Anywhere Environment
###
if [ -d $installDir ]; then
  echo "Install Directory Detected"
else
  mkdir $installDir
  mkdir $installDir/configs
  mkdir $installDir/python
  mkdir $installDir/arduino
  mkdir $installDir/www
fi
cp $configDir/* $installDir/configs
cp -R $pythonDir/* $installDir/python
cp -R $arduinoDir/* $installDir/arduino
cp -R $webDir/* $installDir/www

###
## Set Env Vars
###
