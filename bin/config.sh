#!/bin/sh
scriptDir=/home/pi/Altrubots/python
confDir=/home/pi/Altrubots/configs
ownerConfBase=../configFiles/ownerConfig.txt
botConfBase=../configFiles/botConfig.txt
ownerConfNew=$confDir/ownerConfig.txt
botConfNew=$confDir/botConfig.txt

if [ -e $ownerConfNew ] && [ -e $botConfNew ]; then
   echo "Altrubots seems installed."
   echo "This script will run through 3 screens / User Interfaces"
   echo "The first will be to register you as a bot owner."
   echo "The second will be to register a bot under your owner credentials"
   echo "The final screen will allow you to connect your bot to Altrubots RC Everywhere!"
else
   echo "So, Altrubots RC Anywhere like kinda needs to be installed before you use RC Anywhere...."
   echo "Please confirm configuration files exist at:"
   echo $botConfNew
   echo $ownerConfNew
   echo "Run the install.sh script in this directory to install altrubots if you havent done that yet..."
   exit 1
fi

#grab md5sum of base conf files to compare after a user closes a window
md5OwnerBase=$(md5sum $ownerConfBase | cut -d ' ' -f1)
md5BotBase=$(md5sum $botConfBase | cut -d ' ' -f1)

#run config scripts in apropriate order:
echo "Begining Owner Configuration UI"
python3 $scriptDir/ownerRegistrationUI.py
#once owner conf is run confirm the owner config file is updated
newOwnerMD5=$(md5sum $ownerConfNew | cut -d ' ' -f1)

if [ $md5OwnerBase != $newOwnerMD5 ]; then
   echo "Owner Configuration appears updated. Begining Bot Registration."
else
   echo "Owner Configs do not appear different than base configs..."
   echo "this is unaceptable."
   exit 1
fi

python3 $scriptDir/botRegistrationUI.py
newBotMD5=$(md5sum $botConfNew | cut -d ' ' -f1)
if [ $md5BotBase != $newBotMD5 ]; then
   echo "Bot Configs Are Updated!"
   echo "Now its time to connect your bot to Altrubots!!"
else
   echo "Seriously? You got this far to not register a bot?"
   echo "Kinda weakkkk,,,,"
   exit 1
fi
#all configs should be updated. Unless one of you users gets wiley and changed them manually, the bot should be good to go!
python3 $scriptDir/botConnectionUI.py
