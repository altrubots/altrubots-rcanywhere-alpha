#include <Servo.h>


const byte numChars = 32;
char recievedChars[numChars];
String recv;
boolean newData = false;
int argOne = 0;


int pwmVal;
int pwmServo;

//Motor one controls
Servo servoRightMotor;
//Motor 2 control pins
Servo servoLeftMotor;

//Motor Signal Values
int SignalForward = 1610;
int SignalBackwards= 1340;
int SignalStop= 1500;

//booooost
int SignalForwardBoost = 1850;

//Calibration Relay pins
int powerRelay = 13;
int leftPower = 12;
int rightPower = 11;
int pwmIn = 9;
int pwmServoIn = 10;

int argTwo; // param recieved
//time variable to check for autoshutoff
unsigned long commandTime;
unsigned long curTime;
unsigned long recvTime;

//Change to true if stopped,, functions as the safety
boolean stopped;
boolean goingUp;
boolean goingDown;

boolean forward;
boolean backwards;
boolean left;
boolean right;

//laserz
int laserBurstMax = 5; 
unsigned long laserFireTime = 550;
unsigned long laserPulseInterval = 200;
boolean laserSafety = false;
unsigned long laserTime;
int laserPin = 3;
int laserCount = 0;

//Collector
int liftUpTime =  31000;
int liftDownTimeOne = 30000;
int liftDownDelay = 2000;
int liftDownTimeTwo = 500;
int liftDownDelayTwo = 2000;
int liftDownTimeThree = 2500;
boolean liftSafety = false;
unsigned long liftTime;
int collectorUpPin = 8;
int collectorDownPin = 7;
Servo servoCollector;


void setup(){
  servoRightMotor.attach(5);
  servoLeftMotor.attach(6);
  pinMode(powerRelay, OUTPUT);
  pinMode(pwmIn, INPUT);
  pinMode(rightPower,OUTPUT);
  pinMode(leftPower,OUTPUT);
  pinMode(collectorUpPin, OUTPUT);
  pinMode(collectorDownPin, OUTPUT);
  pinMode(laserPin,OUTPUT);
  Serial.begin(9600);
 //Serial.println("t");
 driveNoMore(); //Stop command off the bat
 commandTime = millis(); 
 laserTime = millis();
 collectorSafetyOn();
 goingUp = false;
 goingDown = false;
// bilgeOff();
safeLasers();
calibrate();
}

void loop(){
  recvData();
  checkNewData();
  safetyCheck();
 // readPrintPwm();
  //readServoPwm();
}



void recvData(){
 static boolean recvInProgress = false;
 static byte ndx = 0;
 char startMarker = '<';
 char endMarker = '>';
 char rc;

 if(Serial.available() > 3){  //7 is smalles
   
  rc = Serial.read();
  if (rc == startMarker){
    recvInProgress = true;
    delay(10); //delay cause im tired of not gettin everything
  }

  while(recvInProgress == true){
    rc = Serial.read(); //read the package 
   if(rc != endMarker){
  // Serial.println("Recvd char:");   
  // Serial.println(rc);
    recievedChars[ndx] = rc;
    ndx++;
    
    //overwrite last digit if too big
    if(ndx >= numChars){
      ndx = numChars - 1;
    }
   }
  if(rc == endMarker){
    //Serial.println('End char Recvd');
    ndx = 0;
    newData = true; //used as trigger for checkNewData method
    recvInProgress = false;
  } 
  }
 } 
  
}

void checkNewData(){
  //Serial.println("New Data");
 if(newData == true){
  argOne = 0;

  //parse out argTwo and Command num
  char * strtokIndx;//index for strtok()

  strtokIndx= strtok(recievedChars,",");
  argOne = atoi(strtokIndx);
  strtokIndx = strtok(NULL,",");
  //Serial.println("ArgOne:");
  //Serial.println(argOne);
  argTwo = atoi(strtokIndx);
  //Serial.println("ArgTwo:");
  //Serial.println(argTwo);
  
 ////call methods based on argOne
 //if needed, pass argTwo into the method called
  if(argOne == 1){
  driveNoMore();
  }
  else if(argOne == 2){
   driveForward(); 
  }
  else if(argOne == 3){
   driveReverse(); 
  }else if(argOne == 4){
    driveLeft();
  }else if(argOne == 5){
   driveRight();
  }else if(argOne == 6){
   collectorUp();
  }else if(argOne == 7){
   collectorDown();//deprecated atm - our bots go down on no one!
  }else if(argOne == 8){
   driveBoost();
  }else if(argOne == 9){
    newSession();
  }else if(argOne == 10){
   lasers();
  }else if(argOne == 11){
   calibrate();
  }
  newData = false;
  //clear arrary:
  memset( recievedChars , 0, sizeof recievedChars);
 } 
}


void driveReverse(){
//  Serial.println("Reverse");
  if(!backwards){
    servoRightMotor.writeMicroseconds(SignalBackwards + 40); //slower reverse
    servoLeftMotor.writeMicroseconds(SignalBackwards + 40); //slower reverse 
    clearDirections();
    backwards = true;
  }else{
     servoRightMotor.writeMicroseconds(SignalBackwards - 15);
    servoLeftMotor.writeMicroseconds(SignalBackwards - 15);
  }

 //digitalWrite(rightPower, HIGH);
 stopped = false;
 commandTime = millis();
}

void driveForward(){
 // Serial.println("Forward");
 if(!forward){
  servoRightMotor.writeMicroseconds(SignalForward);
  servoLeftMotor.writeMicroseconds(SignalForward);
  clearDirections();
  forward = true;
 }else{
  servoRightMotor.writeMicroseconds(SignalForward + 65);
  servoLeftMotor.writeMicroseconds(SignalForward + 65);
 }
 stopped = false;
 commandTime = millis();
}

void driveLeft(){
  Serial.println("Left");
 if(!right){
  servoRightMotor.writeMicroseconds(SignalForward - 40);
  servoLeftMotor.writeMicroseconds(SignalForward + 50);
  clearDirections();
  right = true;
 }else{
  servoRightMotor.writeMicroseconds(SignalBackwards + 35); //weaken
  servoLeftMotor.writeMicroseconds(SignalForward + 45); //strenghten.. otherwise ducks fall out
 }
 stopped = false;
 commandTime = millis() ;
}

void driveRight(){
 // bilgeOn();
 stopped = false;
 if(!left){
  servoRightMotor.writeMicroseconds(SignalForward + 60); //left is a lil weaker on the stryder...
  servoLeftMotor.writeMicroseconds(SignalForward - 40);
  clearDirections();
  left = true;
 }else{
   servoRightMotor.writeMicroseconds(SignalForward + 45);
  servoLeftMotor.writeMicroseconds(SignalBackwards + 35);
 }
 commandTime = millis();
}

void driveNoMore(){
 // Serial.println("Stopping");

  servoRightMotor.writeMicroseconds(SignalStop);
  servoLeftMotor.writeMicroseconds(SignalStop);

}

void driveBoost(){
 // Serial.println("boost");
  driveNoMore();
  delay(200);//experimental to go easier on the speed controllers, if works add2safety
  servoRightMotor.writeMicroseconds(SignalForward);
  servoLeftMotor.writeMicroseconds(SignalForward);
  delay(20);
  servoRightMotor.writeMicroseconds(SignalForwardBoost);
  servoLeftMotor.writeMicroseconds(SignalForwardBoost);
 
 stopped = false;
 commandTime = millis();
}


void readPrintPwm(){
  pwmVal = pulseIn(pwmIn,HIGH);
 Serial.println(pwmVal);
 if(pwmVal < 1000){
    driveLeft();
 }else{
 driveRight();
 }
}


void readServoPwm(){
 pwmServo = pulseIn(pwmServoIn,HIGH);
// Serial.println("Serv:");
 //Serial.println(pwmServo);
 
}


void collectorSafetyOn(){
//collector battery safety
   digitalWrite(collectorUpPin, HIGH);
   digitalWrite(collectorDownPin,HIGH);    
  
}

void collectorUp(){
  //always,, er, NEVER forget to enable your safety!
  Serial.println("up");
 if(!liftSafety){
    Serial.println("LIFT");
    collectorSafetyOn();
    digitalWrite(collectorUpPin, LOW);
    liftTime = millis();
    liftSafety = true;
 }
}

void collectorDown(){
  //always,, er, NEVER forget to enable your safety!
  //Serial.println("down");
   collectorSafetyOn();
   digitalWrite(collectorDownPin, LOW);
//   goingDown = true;
}


void newSession(){
  //new user session, reset the lab
 collectorDown();
}
void calibrate(){
  Serial.println("Calibration...");
    digitalWrite(powerRelay,LOW); //turns relay off to start calibration
    delay(1500);
    digitalWrite(powerRelay,HIGH); // turns relay on 
    servoRightMotor.writeMicroseconds(1500); // send "stop" signal to ESC.
    servoLeftMotor.writeMicroseconds(1500); // send "stop" signal to ESC.
    Serial.println("Begining Calibration...");
    
  delay(7000); // delay to allow the ESC to recognize the stopped signal
  Serial.println("Calibration Complete");
}

void clearDirections(){
  forward = false;
  backwards = false;
  left = false;
  right = false;
}

//TODO: implement a less logic heavy laser burst
void lasers(){
    if(!laserSafety){
       //fire a laser burst!
       //Serial.println("LASERSSS!");
       digitalWrite(laserPin, LOW);
       laserTime = millis();
       laserSafety = true;
       }
}

void safeLasers(){
   digitalWrite(laserPin, HIGH);
}

//Responsible for handling timers and preventing things from turning on/off when they shouldn't
//might want to split up a bit
//TODO: handle 49.7 day millis overflow issue or switch to another means of doing this
void safetyCheck(){
  // handles laser bursting
  if(laserSafety && (millis() - laserTime) > laserFireTime ){
    safeLasers();
    if(laserCount < laserBurstMax && (millis() - laserTime) > (laserFireTime + laserPulseInterval)){
      //todo: remove addition in above if
     // Serial.println("next pulse");
       digitalWrite(laserPin, LOW);
       laserTime = millis();
       laserCount++;
    }else if((millis() -laserTime) > 8000){
      laserCount = 0;
      laserSafety = false;
   }
 }
 //if liftSafety somethings up with the lift
 if(liftSafety){
   //go uptill time 2 go down
   if((liftTime + liftUpTime + liftDownTimeOne) < millis()){
      //   Serial.println("pause - 1st");
         collectorSafetyOn();
         liftSafety = false;
    }else if((liftTime + liftUpTime) < millis()){
       //  Serial.println("go down - 1st");
         collectorDown();
    }















  
  //go uptill time 2 go down
  /*
   if((liftTime + liftUpTime) < millis()){
       Serial.println("go down - 1st");
       collectorDown();
    }else if((liftTime + liftUpTime + liftDownTimeOne) < millis()){
         Serial.println("pause - 1st");
         collectorSafetyOn();
    }else if((liftTime + liftUpTime + liftDownTimeOne + liftDownDelay) < millis()){
         Serial.println("go down - 2nd");
         collectorDown();
    }else if((liftTime + liftUpTime + liftDownTimeOne + liftDownDelay + liftDownTimeTwo) < millis()){
         Serial.println("go down - done");
         collectorSafetyOn();
         liftSafety = false;
    }*/
    
 }
  
}
