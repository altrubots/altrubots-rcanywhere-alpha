//Internet Control of Pan Tilt Laser
//Arduino connected to 2 pwm servos and a relay for laser control
//JHS 2021

#include <Servo.h>

//////////////////////Serial In CONFIGURATION///////////////////////////////
const byte numChars = 32;      //max msg size
char recievedChars[numChars];  //where msgs go from pc
String recv;
boolean newData = false;      //was data found to read?
//TODO: Move to array of size CHANNEL_NUMBER
int argNums = 2;
int input [2];
int argOne = 0;               //what first arg of message gets turned to
int argTwo;                   // param recieved



/////////////////Output Congig ////////////////////////////////////////////
//Motor one controls
Servo servoPan;
//Motor 2 control pins
Servo servoTilt;
int panServoPin = 10;
int tiltServoPin = 11;
int mid = 1500;
int panVal = 1500;
int tiltVal = 1500;
//ONLY EVER MOVE BY INCREMENT OR CENTERING WILL BREAK
int panIncrement = 5;
int tiltIncrement = 5;
int minIncrement = 2;
int maxIncrement = 20;

int panLimit = 100;
int tiltLimit = 100;
int panMax = mid + panLimit;
int panMin = mid - panLimit;
int tiltMax = mid + tiltLimit;
int tiltMin = mid - tiltLimit;

////////////////Time Slip for Relays, Misc////////////////////////////////
int lastTime = 0;
int lastLaserTime = 0;
int laserDuration = 500;
boolean laserActive = false;
 //currently using a pwm activated relay, so laser is turned on/off with a high pwm out, and turned off with a low output
Servo servoLaserRelay;
int laserRelayPin = 9;
int laserRelayOnVal = 1800;
int laserRelayOffVal = 1100;
//To make the centering more smooth I have added a timeslip for it
//this makes the value being written not just jump to center position but move towards it in small iterations
boolean centering = false;
int slipCount = 10;
int centerCount = 0;

void setup() {
  servoPan.attach(panServoPin);
  servoTilt.attach(tiltServoPin);
  servoLaserRelay.attach(laserRelayPin);

  Serial.begin(9600);
}

void loop() {
   recvData();
  handleNewData();

}




void recvData(){
 static boolean recvInProgress = false;
 static byte ndx = 0;
 char startMarker = '<';
 char endMarker = '>';
 char rc;

 if(Serial.available() > 3){  //3 is smalles

  rc = Serial.read();
  if (rc == startMarker){
    recvInProgress = true;
    delay(10); //Todo move 2 safety?/y no worky well?
  }

  while(recvInProgress == true){
    rc = Serial.read(); //read the package
   if(rc != endMarker){
  // Serial.println("Recvd char:");
  // Serial.println(rc);
    recievedChars[ndx] = rc;
    ndx++;

    //overwrite last digit if too big
    if(ndx >= numChars){
      ndx = numChars - 1;
    }
   }
  if(rc == endMarker){
    //Serial.println('End char Recvd');
    ndx = 0;
    newData = true; //used as trigger for handleNewData method
    recvInProgress = false;
  }
  }
 }

}







void center(){
//  Serial.println("centering");
  servoPan.writeMicroseconds(mid);
  servoTilt.writeMicroseconds(mid);
  centering = true;

}

void moveDown(){
//  Serial.println("up");
  if(tiltVal < tiltMax){
    tiltVal += tiltIncrement;
  }else{
  //  Serial.println("max");
  }

  servoTilt.writeMicroseconds(tiltVal);

}


void moveUp(){
 // Serial.println("down");
  if(tiltVal > tiltMin){
  tiltVal -= tiltIncrement;
  }else{
 //   Serial.println("Min");
  }
  servoTilt.writeMicroseconds(tiltVal);

}


void moveLeft(){
 // Serial.println("left");
  if(panVal < panMax){
  panVal += panIncrement;
  }else{
  //  Serial.println("max");
  }
  servoPan.writeMicroseconds(panVal);

}

void moveRight(){
  //Serial.println("right");
  if(panVal > panMin){
  panVal -= panIncrement;
  }
  servoPan.writeMicroseconds(panVal);

}

void increasePTIncrement(){
   if(panIncrement < maxIncrement){
       panIncrement = panIncrement + 2;
       tiltIncrement = tiltIncrement + 2;
   }
   
}

void decreasePTIncrement(){
   if(panIncrement > minIncrement){
       panIncrement = panIncrement - 2;
       tiltIncrement = tiltIncrement - 2;
   }
   
}

void laserOn(){
   //Serial.println("l");
    lastLaserTime = millis();
    laserActive = true;
    //turn on laser
    servoLaserRelay.writeMicroseconds(laserRelayOnVal);

}
void laserOff(){
//   Serial.println("l");
     laserActive = false;
    servoLaserRelay.writeMicroseconds(laserRelayOffVal);
}

void handleNewData(){
 // Serial.println("New Data");
 if(newData == true){
  argOne = 0;

  //parse out argTwo and Command num
  char * strtokIndx;//index for strtok()

  strtokIndx= strtok(recievedChars,"-");
  argOne = atoi(strtokIndx);
  strtokIndx = strtok(NULL,"-");
 // Serial.println("ArgOne:");
  //Serial.println(argOne);
  argTwo = atoi(strtokIndx);
 // Serial.println("ArgTwo:");
 // Serial.println(argTwo);

  if(argOne == 1){
 // center();
    centering = true;
  }
  else if(argOne == 2){
   moveUp();
   centering = false;
  }
   else if(argOne == 3){
   moveDown();
   centering = false;
  }else if(argOne == 4){
    moveLeft();
    centering = false;
  }else if(argOne == 5){
   moveRight();
   centering = false;
  }else if(argOne == 6){
   laserOn();
   }else if(argOne == 7){
    laserOff();
   }
   else if(argOne == 8){
     increasePTIncrement();
   }
   else if(argOne == 9){
      decreasePTIncrement();
   }
}
  newData = false;
  //clear arrary:
  memset( recievedChars , 0, sizeof recievedChars);
 }

boolean checkCenter(int x, int y){
   if(x == mid && y == mid){
      return true;
   }
   return false;
}


 void timeSlip(){
  centerCount++;
   if(laserActive){
      //check if time to turn off
      if((lastLaserTime + laserDuration) < millis()){
         //laser passed duration, turn off
         laserOff();
      }
   }
   if(centering){
      if(checkCenter(panVal, tiltVal)){
         centering= false;
      }else{
        if(centerCount > slipCount){
           centerCount = 0;
           //move to center
           if(panVal > mid){
               moveRight();
            }
           if(panVal < mid){
              moveLeft();
           }if(tiltVal > mid){
            moveUp();
          }
          if(tiltVal < mid){
            moveDown();
         }
      }
    }
   }
}
