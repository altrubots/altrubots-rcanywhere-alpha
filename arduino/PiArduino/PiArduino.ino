//This is the C++ Arduino Project for Altrubots RC Everywhere Telepresence.
//This can be run on an arduino hooked up to a rapberry pi.
//See: https://altrubots.com/raspberry-pi-telepresence.php for more details
#include <Servo.h>


const byte numChars = 32;
char recievedChars[numChars];
String recv;
boolean newData = false;
int argOne = 0;


int pwmVal;
int pwmServo;

//Motor one controls
Servo servoRightMotor;
int rightPinNum = 5;
//Motor 2 controls
Servo servoLeftMotor;
int leftPinNum = 6;

//Motor Signal Values
int SignalForward = 1650;
int SignalBackwards= 1350;
int SignalStop= 1500;

//booooost
int SignalForwardBoost = 1850;

int argTwo; // param recieved from pi

//time variable to check for autoshutoff
unsigned long commandTime;
unsigned long curTime;
unsigned long recvTime;

//Change to true if stopped,, functions as the safety and maintains state
boolean stopped;
boolean forward;
boolean backwards;
boolean left;
boolean right;




void setup(){
  servoRightMotor.attach(rightPinNum);
  servoLeftMotor.attach(leftPinNum);

  Serial.begin(9600);

 driveNoMore(); //full stop the bot right off the bat
 commandTime = millis();

 calibrate(); //Some ESC's need a calibration period to maintain a base no move pwm.
}

void loop(){
  recvData();     //get serial data from pi
  checkNewData(); //parse new data, if its available. Act if needed
  safetyCheck();  //Time slip checks to turn any features off. Allows incremental driving instead of continuouss

}



void recvData(){
 static boolean recvInProgress = false;
 static byte ndx = 0;
 char startMarker = '<';
 char endMarker = '>';
 char rc;

 if(Serial.available() > 3){

  rc = Serial.read();
  if (rc == startMarker){
    recvInProgress = true;
    delay(10); //delay cause im tired of not gettin everything.. should really up the baud rate..
  }

  while(recvInProgress == true){
    rc = Serial.read(); //read the package
   if(rc != endMarker){
    recievedChars[ndx] = rc;
    ndx++;

    //overwrite last digit if too big
    if(ndx >= numChars){
      ndx = numChars - 1;
    }
   }
  if(rc == endMarker){
    ndx = 0;
    newData = true; //used as trigger for checkNewData method
    recvInProgress = false;
  }
  }
 }

}

void checkNewData(){

 if(newData == true){
   //if data was recieved, act on it.
  argOne = 0;

  //parse out argTwo and Command num
  char * strtokIndx;//index for strtok()

  strtokIndx= strtok(recievedChars,",");
  argOne = atoi(strtokIndx);
  strtokIndx = strtok(NULL,",");
  argTwo = atoi(strtokIndx);


 ////call methods based on argOne
 //if needed, pass argTwo into the method called
  if(argOne == 1){
  driveNoMore();
  }
  else if(argOne == 2){
   driveForward();
  }
  else if(argOne == 3){
   driveReverse();
  }else if(argOne == 4){
    driveLeft();
  }else if(argOne == 5){
   driveRight();
  }else if(argOne == 6){
  // collectorUp(); //disable atm... though have no fear - Altrubots is on the way up!
  }else if(argOne == 7){
  // collectorDown();//deprecated atm - our bots go down on no one!
  }else if(argOne == 8){
   driveBoost();
  }else if(argOne == 9){
    newSession();
  }else if(argOne == 10){
  // lasers();
  }else if(argOne == 11){
   calibrate();
  }
  newData = false;
  //clear arrary:
  memset( recievedChars , 0, sizeof recievedChars);
 }
}


//Configure SignalBackwards and SignalForward to control speed!
void driveReverse(){

  if(!backwards){
    servoRightMotor.writeMicroseconds(SignalBackwards); //slower reverse
    servoLeftMotor.writeMicroseconds(SignalBackwards); //slower reverse
    clearDirections();
    backwards = true;
  }else{
     servoRightMotor.writeMicroseconds(SignalBackwards - 35);
    servoLeftMotor.writeMicroseconds(SignalBackwards - 35);
  }
 stopped = false;
 commandTime = millis();
}

void driveForward(){

 if(!forward){
  servoRightMotor.writeMicroseconds(SignalForward);
  servoLeftMotor.writeMicroseconds(SignalForward);
  clearDirections();
  forward = true;
 }else{
  servoRightMotor.writeMicroseconds(SignalForward + 65);
  servoLeftMotor.writeMicroseconds(SignalForward + 65);
 }
 stopped = false;
 commandTime = millis();
}


//Drive Right and Drive left have a default of two drive options
//Upon Rcving the first right/left message the bot will veer in that direction
//Subsequent calls move the bot further in that direction.
//In this case the second call switches to a zero point turn in that direction
void driveRight(){

  stopped = false;
 if(!right){
  servoRightMotor.writeMicroseconds(SignalForward - 50);
  servoLeftMotor.writeMicroseconds(SignalForward + 50);
  clearDirections();
  right = true;
 }else{
  servoRightMotor.writeMicroseconds(SignalBackwards + 45);
  servoLeftMotor.writeMicroseconds(SignalForward + 45);
 }
 commandTime = millis() ;
}

//Please hack these drive methods to make them run better with your bots!
//Changing the values for signalForward and signalReverse, as well as changing the
//values added/subtracted to them can modify your bots performance
void driveLeft(){
 stopped = false;
 if(!left){
  servoRightMotor.writeMicroseconds(SignalForward + 60);
  servoLeftMotor.writeMicroseconds(SignalForward - 40);
  clearDirections();
  left = true;
 }else{
   servoRightMotor.writeMicroseconds(SignalForward + 45);
  servoLeftMotor.writeMicroseconds(SignalBackwards + 35);
 }
 commandTime = millis();
}


//STOP
void driveNoMore(){
  servoRightMotor.writeMicroseconds(SignalStop);
  servoLeftMotor.writeMicroseconds(SignalStop);
  stopped = true;
}



void driveBoost(){
  driveNoMore();
  delay(100);//experimental to go easier on the speed controllers, if works add2safety
  servoRightMotor.writeMicroseconds(SignalForward);
  servoLeftMotor.writeMicroseconds(SignalForward);
  delay(5);
  servoRightMotor.writeMicroseconds(SignalForwardBoost);
  servoLeftMotor.writeMicroseconds(SignalForwardBoost);

 stopped = false;
 commandTime = millis();
}


void newSession(){
  //new user session, reset the lab
 //handle any reset actions here
}
void calibrate(){

    Serial.println("Calibration...");
    //addition of a relay allows escs to be recalibrated post boot. This is highly recomended if calibration is needed
    //ESPECIALLY TRUE FOR 3 WIRE RC ESC's
    //digitalWrite(powerRelay,LOW); //turns relay off to start calibration
   // delay(1500);
   // digitalWrite(powerRelay,HIGH); // turns relay on
    servoRightMotor.writeMicroseconds(SignalStop); // send "stop" signal to ESC.
    servoLeftMotor.writeMicroseconds(SignalStop); // send "stop" signal to ESC.
    Serial.println("Begining Calibration...");
    delay(7000); // delay to allow the ESC to recognize the stopped signal
    Serial.println("Calibration Complete");
}

void clearDirections(){
  forward = false;
  backwards = false;
  left = false;
  right = false;
}



//Responsible for handling timers and preventing things from turning on/off when they shouldn't
//might want to split up a bit
//See the Romulus Lab Bot Project for examples
void safetyCheck(){

}
