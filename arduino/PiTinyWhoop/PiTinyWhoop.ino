//Internet Control of R/C Equuipment
//Control Side, Tiny Whoop / Generic PPM Quadcopter
//JHS 2020


//////////////////////Serial In CONFIGURATION///////////////////////////////
const byte numChars = 32;      //max msg size
char recievedChars[numChars];  //where msgs go from pc
String recv;
boolean newData = false;      //was data found to read?
int argOne = 0;               //what first arg of message gets turned to
int argTwo;                   // param recieved
int argThree;
int argFour;
int argFive;
int argSix;


//////////////////////PPM CONFIGURATION///////////////////////////////
#define CHANNEL_NUMBER 8  //set the number of chanels
#define CHANNEL_DEFAULT_VALUE 1500  //set the default servo value
#define FRAME_LENGTH 22500  //set the PPM frame length in microseconds (1ms = 1000µs)
#define PULSE_LENGTH 300  //set the pulse length
#define onState 1  //set polarity of the pulses: 1 is positive, 0 is negative
#define sigPin 10  //set PPM signal output pin on the arduino
/*this array holds the servo values for the ppm signal)*/
int ppm[CHANNEL_NUMBER];

//////////////////////Output CONFIGURATION///////////////////////////////
int stopVal = 1500;
int forwardVal = 1600;
int forwardHighVal = 1700;
int forwardBoostVal = 1800;
int boostVal = 1800;
int reverseVal = 1390;
int reverseHighVal = 1350;
int forwardCount = 0;
int reverseCount = 0;
int leftCount = 0;
int rightCount = 0;
//for Steering Time Slipping
int moveTime = 400;
unsigned long leftAcuatorStartTime;
unsigned long rightActuatorStartTime;
boolean movingRight = false;
boolean movingLeft = false;
boolean movingForward = false;
boolean movingReverse = false;


int relayOff = 1400;
int relayOn = 1100;

//which pwm channel the sabertooth is hooked up too
int throttleChannel  = 3;
int RudderChannel = 4;
int elevatorChannel = 2;
int aileronChannel = 1;
int armButton = 5;
int modeSwitch = 6;


void setup(){
  Serial.begin(9600);
//  driveNoMore(); //Stop command off the bat, not neccessarilly needed cause of default pwm vals. a calibration function might b needed tho


//initiallize default ppm values
for(int i=0; i<CHANNEL_NUMBER; i++){
ppm[i]= CHANNEL_DEFAULT_VALUE;
}

  pinMode(sigPin, OUTPUT);
  digitalWrite(sigPin, !onState);  //set the PPM signal pin to the default state (off)
  cli();
  TCCR1A = 0; // set entire TCCR1 register to 0
  TCCR1B = 0;
  OCR1A = 100;  // compare match register, change this
  TCCR1B |= (1 << WGM12);  // turn on CTC mode
  TCCR1B |= (1 << CS11);  // 8 prescaler: 0,5 microseconds at 16mhz
  TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt
  sei();
}

void loop(){
   //read from serial
    recvData();
   //and update the ppm array accordingly
   handleNewData();
   timeSlip();
}



void recvData(){
 static boolean recvInProgress = false;
 static byte ndx = 0;
 char startMarker = '<';
 char endMarker = '>';
 char rc;

 if(Serial.available() > 3){  //3 is smalles

  rc = Serial.read();
  if (rc == startMarker){
    recvInProgress = true;
    delay(10); //Todo move 2 safety?/y no worky well?
  }

  while(recvInProgress == true){
    rc = Serial.read(); //read the package
   if(rc != endMarker){
  // Serial.println("Recvd char:");
  // Serial.println(rc);
    recievedChars[ndx] = rc;
    ndx++;

    //overwrite last digit if too big
    if(ndx >= numChars){
      ndx = numChars - 1;
    }
   }
  if(rc == endMarker){
    //Serial.println('End char Recvd');
    ndx = 0;
    newData = true; //used as trigger for handleNewData method
    recvInProgress = false;
  }
  }
 }

}




void handleNewData(){
 // Serial.println("New Data");
 if(newData == true){
  argOne = 0;

  //parse out argTwo and Command num
  char * strtokIndx;//index for strtok()

  strtokIndx= strtok(recievedChars,"-");
  argOne = atoi(strtokIndx);
  strtokIndx = strtok(NULL,"-");
  Serial.println("ArgOne:");
  Serial.println(argOne);
  argTwo = atoi(strtokIndx);
  Serial.println("ArgTwo:");
  Serial.println(argTwo);
  ppmArray[0] = argOne;
  ppmArrary[1] = argTwo;
  argThree = atoi(strtokIndx);
  Serial.println("ArgThree:");
  Serial.println(argThree);
  ppmArray[2] = argThree;
 ////call methods based on argOne
 //if needed, pass argTwo into the method called
 //do stuff based on input
  if(argOne == 1){
  driveNoMore();
  }
  else if(argOne == 2){
   driveForward();
  }
   else if(argOne == 3){
   driveReverse();
  }else if(argOne == 4){
    driveLeft();
  }else if(argOne == 5){
   driveRight();
  }else if(argOne == 6){
   frontPumpsOn();
   }else if(argOne == 7){
    frontPumpsOff();
   }else if(argOne == 8){
    boost();
   }else if(argOne == 9){
    backPumpsOff();
}
  newData = false;
  //clear arrary:
  memset( recievedChars , 0, sizeof recievedChars);
 }
}

void resetCounts(){
   forwardCount = 0;
   reverseCount = 0;
   leftCount = 0;
   rightCount = 0;
   //also, reset dir bools
   movingForward = false;
   movingReverse = false;
}





 void driveNoMore(){
  Serial.println("Stop - 1500's");
  ppm[0] = stopVal;
  ppm[1] = stopVal;
  ppm[2] = stopVal;
  ppm[3] = stopVal;
  ppm[4] = stopVal;
  ppm[5] = stopVal;
  ppm[6] = stopVal;
  ppm[7] = stopVal;
  resetCounts();
 }

 void driveForward(){
    if(forwardCount == 0){
      resetCounts();
      ppm[throttleChannel] = forwardVal;
      forwardCount++;
      movingForward = true;
   }else if(forwardCount == 1){
      ppm[throttleChannel] = forwardHighVal;
      forwardCount++;
   }

 }

 void driveLeft(){
   //Serial.println("left");
   leftAcuatorStartTime = millis();
   movingRight = false;
   movingLeft = true;
   updateThrottleForSteering();
   ppm[steeringChannel] = 1200;
 }


 void driveRight(){
  // Serial.println("right");
   rightActuatorStartTime = millis();
   movingRight = true;
   movingLeft = false;
   updateThrottleForSteering();
   ppm[steeringChannel] = 1800;
 }

 void updateThrottleForSteering(){
    if(movingForward){
      ppm[throttleChannel] = forwardVal; //turn at low speed mode
   }else if(movingReverse){
      ppm[throttleChannel] = reverseVal; //turn at low speed mode
   }
}

  void driveReverse(){
    if(reverseCount == 0){
      resetCounts();
      ppm[throttleChannel] = reverseVal;
      reverseCount++;
      movingReverse = true;
   }else if(reverseCount == 1){
      //not so fast plz when u cant see....
      ppm[throttleChannel] = reverseHighVal + 50;
      reverseCount++;
   }

 }

 void boost(){
    resetCounts();
    ppm[throttleChannel] = boostVal;
}

 void frontPumpsOn(){
   ppm[3] = relayOn;
}

void frontPumpsOff(){
   ppm[3] = relayOff;
}

void backPumpsOn(){
   ppm[4] = relayOn;
}

void backPumpsOff(){
   ppm[4] = relayOff;
}


void timeSlip(){
   if(movingLeft){
      if(leftAcuatorStartTime < (millis() - moveTime)){
         //stop moving left
         ppm[steeringChannel] = stopVal;
         movingLeft = false;
     //    Serial.println("Done moving left");
      }
   }
   if(movingRight){
  //  Serial.println("moving right");
      if(rightActuatorStartTime < (millis() - moveTime)){
         //stop moving right
         ppm[steeringChannel] = stopVal;
         movingRight = false;
        // Serial.println("Done moving right");
      }
   }
}


//TODO: move this stuff to helper .c/.h files
//uh uh uh, dont touch anything below this line
//use the vars in the ppwm config to configure
/*
 * PPM generator originally written by David Hasko
*/
ISR(TIMER1_COMPA_vect){  //leave this alone

  static boolean state = true;



  TCNT1 = 0;



  if (state) {  //start pulse

    digitalWrite(sigPin, onState);

    OCR1A = PULSE_LENGTH * 2;

    state = false;

  } else{  //end pulse and calculate when to start the next pulse

    static byte cur_chan_numb;

    static unsigned int calc_rest;



    digitalWrite(sigPin, !onState);

    state = true;



    if(cur_chan_numb >= CHANNEL_NUMBER){

      cur_chan_numb = 0;

      calc_rest = calc_rest + PULSE_LENGTH;//

      OCR1A = (FRAME_LENGTH - calc_rest) * 2;

      calc_rest = 0;

    }

    else{

      OCR1A = (ppm[cur_chan_numb] - PULSE_LENGTH) * 2;

      calc_rest = calc_rest + ppm[cur_chan_numb];

      cur_chan_numb++;

    }

  }

}
